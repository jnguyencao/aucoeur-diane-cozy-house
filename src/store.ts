import { configureStore } from '@reduxjs/toolkit'
import demandContactReducer from './components/contact/demandContactSlice'
import accommodationReducer from './components/home/accommodation/redux/accommodationSlice'

export const store = configureStore({
  reducer: {
    demandContact: demandContactReducer,
    accommodation: accommodationReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch