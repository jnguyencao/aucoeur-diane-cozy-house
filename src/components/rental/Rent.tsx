import React, { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useParams } from "react-router"
import Skeleton from "react-loading-skeleton"
import { FaStar } from "react-icons/fa"
import { useTranslation } from 'react-i18next'
import dayjs, { Dayjs } from 'dayjs'
import { list } from '../data/Data'
import './rent.scss'
import { Button, Select, Form, Input, DatePicker, InputNumber, Carousel, Modal } from 'antd'
import { getAccommodation, idleAccommodations } from "../home/accommodation/redux/accommodationSlice"
import { AppDispatch, RootState } from '../../store'

const { RangePicker } = DatePicker

const Rent = () => {
  const { t, i18n } = useTranslation('accommodations')
  const { id } = useParams<{ id: string }>()
  const dispatch: AppDispatch = useDispatch()
  const apiURL = process.env.REACT_APP_API_URL
  const accommodationFromRedux = useSelector((state: RootState) => state.accommodation.accommodation)
  const getAccommodationStatus = useSelector((state: RootState) => state.accommodation.getAccommodationStatus)
  const email = t("Your email")
  const fullName = t("Your name")
  const [imageSrc, setImageSrc] = useState<string[]>([])
  const [name, setName] = useState<string>()
  const [availability, setAvailability] = useState<string>()
  const [price, setPrice] = useState<number>()
  const [house, setHouse] = useState<string>()
  const [capacity, setCapacity] = useState<number>()
  const [dates, setDates] = useState<[Dayjs, Dayjs] | null>(null)
  const [submitting, setSubmitting] = useState(false)
  const [previewVisible, setPreviewVisible] = useState(false)
  const [previewImage, setPreviewImage] = useState('')

  useEffect(() => {
    if (apiURL !== "https://aucoeur-back-main.onrender.com" && id) {
      dispatch(getAccommodation({ accommodationId: id }))
    }
    return () => {
      dispatch(idleAccommodations())
    }
  }, [dispatch, apiURL, id])

  useEffect(() => {
    const accommodation = list.find(val => val._id === id)
    const item = apiURL === "https://aucoeur-back-main.onrender.com" ? accommodation : accommodationFromRedux
    if (item && id) {
      setName(item.name)
      setAvailability(item.active ? 'Available' : 'Unavailable')
      setPrice(item.price)
      setCapacity(item.capacity)
      setHouse((item.house))
      if (item.cover)
        setImageSrc(item.additionalImages ? [item.cover, ...item.additionalImages] : [item.cover])
    } else {
      setImageSrc(['../images/about.jpg'])
      setName('Unknown')
      setAvailability('Unknown')
      setPrice(0)
      setCapacity(0)
      setHouse('Unknown')
    }
  }, [id, t, i18n.language, accommodationFromRedux, apiURL])

  const handlePreview = (src: string) => {
    setPreviewImage(src)
    setPreviewVisible(true)
  }

  const Loading = () => {
    return (
      <>
        <div className="col-md-6 skeleton">
          <Skeleton height={400} />
        </div>
        <div className="col-md-6 skeleton" style={{ lineHeight: 2 }}>
          <Skeleton height={50} width={300} />
          <Skeleton height={75} />
          <Skeleton height={25} width={150} />
          <Skeleton height={50} />
          <Skeleton height={150} />
          <Skeleton height={50} width={100} />
          <Skeleton height={50} width={100} style={{ marginLeft: 6 }} />
        </div>
      </>
    )
  };

  const ShowProduct = () => {
    return (
      <>
       <div className="col-md-6">
        <Carousel
          autoplay
          draggable
          arrows
        >
          {imageSrc.map((src, index) => (
            <div key={index} onClick={() => handlePreview(src)}>
              <img
                src={src}
                alt={name}
                className="carousel-image"
              />
            </div>
           ))}
        </Carousel>
       </div>
        <div className="col-md-6">
          <h4 className="text-uppercase text-black-50">{availability}</h4>
          <h1 className="display-5">{name}</h1>
          <p className="lead fw-bolder">
            {house}
            <FaStar />
          </p>
          <h3 className="display-6 fw-bold my-4">{price} €</h3>
          <p className="lead">{t('For_people', { count: capacity })}</p>
          <div className='form box'>
            <span>{t('Ask us for reservation')}</span>
            <Form
              autoComplete="off"
              layout="inline"
              disabled={submitting}
            >
              <Form.Item
                name="dates"
                rules={[{ required: true, message: t('Please input dates') || "" }]}
              >
                <RangePicker
                  showTime
                  format="DD/MM/YYYY HH:mm"
                  inputReadOnly
                  placeholder={[t('Start date'), t('End date')]}
                  disabled={submitting}
                />
              </Form.Item>
              <Form.Item
                name="occupants"
              >
                <InputNumber
                  min={1}
                  max={60}
                  style={{ width: '7.5em' }}
                  addonAfter={t('pers', { ns: 'common' })}
                />
              </Form.Item>
              <Form.Item name="fullname" rules={[{ required: true, message: t('Please input your name') as string }]}>
                <Input placeholder={fullName}  disabled={submitting} />
              </Form.Item>
              <Form.Item
                name="email"
                rules={[
                  { required: true, message: t('Please enter your email') as string },
                  { type: 'email', message: t('Please enter a valid email address') as string, validateTrigger: 'onSubmit' }
                ]}
              >
                <Input placeholder={email}  disabled={submitting} />
              </Form.Item>
              <Button
                type="primary"
                htmlType="submit"
              >
                {t('Send')}
              </Button>
            </Form>
          </div>
        </div>
        <Modal
           className="custom-modal"
           open={previewVisible}
           footer={null}
           onCancel={() => setPreviewVisible(false)}
        >
           <img alt="preview" src={previewImage} />
        </Modal>
      </>
    )
  }

  return (
    <div>
      <div className="container py-5">
        <div className="row py-4">
          {getAccommodationStatus === 'loading' ? <Loading /> : <ShowProduct />}
        </div>
      </div>
    </div>
  )
}

export default Rent
