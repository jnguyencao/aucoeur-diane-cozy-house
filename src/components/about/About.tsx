import React from "react"
import Heading from "../common/Heading"
import "./about.scss"
import { Button } from 'antd'
import { useTranslation } from 'react-i18next'

const About = () => {
  const { t } = useTranslation('about')
  const redirectToServicePage = () => {
    window.open('https://aucoeurservice.fr', '_blank')
  }

  return (
    <>
      <section className='about'>
        <div className="back">
          <div className="container">
            <h1 className="intro">{t('About Us')}</h1>
          </div>
          <img src='./images/back.jpg' alt="" />
        </div>
        <div className='container flex mtop'>
          <div className='left row'>
            <Heading title="Cosy Nest Realty" subtitle={t('Welcome to our premium rental service.')} />
            <p>{t('Our website, managed by Aucoeur Service, specializes in offering rental properties ranging from cozy studios to luxurious and modern apartments. We are always ready to meet your needs with a diverse selection, ensuring the utmost comfort and convenience.')}</p>
            <p>{t('In addition, we provide complementary services such as airport transfers, taxi services, and parking rentals, ensuring you have the best and most convenient experience. Not stopping there, we also offer tour guide services to help you explore exciting and attractive destinations.')}</p>
            <p>{t('With a commitment to dedicated and professional service, we strive to bring absolute satisfaction to our customers. Let us accompany you on your journey to find the ideal living space and enjoy the best amenities.')}</p>    
            <Button type="primary" className='btn2' onClick={redirectToServicePage} size="large">
              {t('About Aucoeur Service')}
            </Button>
          </div>
          <div className='right row'>
            <img src='./images/about.jpg' alt='' />
          </div>
        </div>
      </section>
    </>
  )
}

export default About
