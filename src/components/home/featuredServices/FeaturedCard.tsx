import { featured } from "../../data/Data"
import { useTranslation } from 'react-i18next'
import { useNavigate } from "react-router-dom"
import { FaTruckPlane } from "react-icons/fa6"
import { FaCarSide } from "react-icons/fa"
import { MdTour } from "react-icons/md"

const FeaturedCard = () => {
  const { t } = useTranslation('services')

  return (
    <>
      <div className='content mtop'>
        {featured.map((items, index) => (
          <div className='box' key={index}>
            {items.cover === 'plane' 
            ? (
              <FaTruckPlane size={50}/>
              ) 
            : ( items.cover === 'car' 
              ? (
                <FaCarSide size={50}/>
              )
              : (
              <MdTour size={50}/>
              )
            )
            }
            <h4>{t(items.name)}</h4>
            <label>{t(items.notice)}</label>
          </div>
        ))}
      </div>
    </>
  )
}

export default FeaturedCard
