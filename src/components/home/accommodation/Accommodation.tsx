import React, { useState, useEffect, useRef } from "react"
import Heading from "../../common/Heading"
import "./accommodation.scss"
import RoomCard from "./RoomCard"
import { useTranslation } from 'react-i18next'
import { useNavigate } from "react-router-dom"
import APPLICATIONS from "../../data/Applications"

const Accommodation = () => {
  const applicationId = APPLICATIONS[0].id
  const { t } = useTranslation('home')
  const navigate = useNavigate()

  const handleShowRooms = () => {
    navigate('/accommodations')
  }
  return (
    <>
      <section className='accommodation padding'>
        <div className='container'>
          <Heading title={t('Our accommodations',{ns: 'accommodations'})} subtitle={t('Experience the pinnacle of grandeur in our accommodations. Live your finest moments with Le Corvier Castle.')} />
          <div className={`accommodation-container`}>
            <RoomCard applicationId={applicationId} />
          </div>
          <div className="button-container">
            <button onClick={handleShowRooms} className="show-accommodations-button">
              {t('Show More')}
            </button>
          </div>
        </div>
      </section>
    </>
  )
}

export default Accommodation
