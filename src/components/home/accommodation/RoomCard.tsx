import React, { useEffect } from 'react'
import { Button, Spin, Carousel } from 'antd'
import { useTranslation } from 'react-i18next'
import { useNavigate } from "react-router-dom"
import { AppDispatch, RootState } from '../../../store'
import { useDispatch, useSelector } from 'react-redux'
import { ApplicationAccommodation, fetchAccommodations, idleAccommodations } from './redux/accommodationSlice'
import { IoBed } from "react-icons/io5"
import { MdPerson } from "react-icons/md"
import { IoMdHeart } from "react-icons/io"
import { list } from '../../data/Data'
import './accommodation.scss'

interface Props {
  applicationId: string
}

function RoomCard({ applicationId }: Props) {
  const dispatch: AppDispatch = useDispatch()
  const { t } = useTranslation('accommodations')
  const navigate = useNavigate()
  const apiURL = process.env.REACT_APP_API_URL
  const accommodationsFromRedux = useSelector((state: RootState) => state.accommodation.accommodations)
  const fetchAccommodationsStatus = useSelector((state: RootState) => state.accommodation.fetchAccommodationsStatus)

  const redirectToServicePage = (id: string | undefined) => {
    navigate(`/rent/${id}`)
    window.scrollTo(0, 0)
  }

  useEffect(() => {
    if (apiURL !== "https://aucoeur-back-main.onrender.com" && applicationId) {
      dispatch(fetchAccommodations({ applicationId }))
    }
    return () => {
      dispatch(idleAccommodations())
    }
  }, [applicationId, dispatch, apiURL])

  
  const accommodations = apiURL === "https://aucoeur-back-main.onrender.com" ? list : accommodationsFromRedux

  console.log("Accommodations:", accommodations)

  const getImages = (cover: string, additionalImages: string[] | undefined): string[] => {
    return additionalImages ? [cover, ...additionalImages] : [cover]
  }

  return (
    <div className='accommodation content grid3 mtop'>
      {accommodations.map((val: ApplicationAccommodation, index: number) => {
        const { name, _id, price, cover, active, capacity, house, additionalImages } = val
        const coverImg = cover ? cover : "../images/about.jpg"
        const additionalImg = additionalImages ? additionalImages : []
        const images = getImages(coverImg, additionalImg)
        return (
          <div
            className="box shadow"
            key={index}
            onClick={() => redirectToServicePage(_id)}
          >
            <div onClick={(e) => e.stopPropagation()}>
             <Carousel
                arrows
                autoplay
                dots={images.length > 1}
                draggable
              >
                {images.map((image, i) => (
                  <div key={i}>
                    <img src={image} alt="" className="carousel-img" />
                  </div>
                ))}
              </Carousel>
            </div>
            <div className="text clickable">
              <div className="category flex">
                <span
                  style={{
                    background: active ? '#25b5791a' : '#ff98001a',
                    color: active ? '#25b579' : '#ff9800',
                  }}
                >
                  {active ? t('Available') : t('Unavailable')}
                </span>
              </div>
              <h4>{house} {name}</h4>
            </div>
            <div className='button flex clickable'>
              <div>
                <Button type="primary" className="btn2" size="large">
                  {price} €
                </Button>
                <label>/{t('night')}</label>
              </div>
              <span>
                {t('For_people', { count: capacity })}
              </span>
            </div>
          </div>
        )
      })}
      {fetchAccommodationsStatus !== 'success' && apiURL !== "https://aucoeur-back-main.onrender.com" && (
        <div className="loading-container">
          <Spin />
        </div>
      )}
    </div>
  )
}

export default RoomCard
