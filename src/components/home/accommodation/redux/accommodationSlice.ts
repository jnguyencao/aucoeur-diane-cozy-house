import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { FetchStatus } from "../../../contact/demandContactSlice"

export interface ApplicationAccommodation {
  applicationId: string
  name: string
  numBeds: number
  capacity: number
  active: boolean
  cover?: string
  house?: string
  additionalImages?: string[]
  type?: string
  category?: string
  description?: string
  price?: number
  startDate?: Date
  createdAt?: Date
  floor?: number
  _id?: string
}

interface State {
  accommodationsTotalNumber: number
  accommodation: ApplicationAccommodation | undefined
  accommodations: ApplicationAccommodation[]
  getAccommodationStatus: FetchStatus
  getAccommodationError?: string
  fetchAccommodationsStatus: FetchStatus
  fetchAccommodationsError?: string
}

const initialState: State = {
  accommodationsTotalNumber: 0,
  accommodation: undefined,
  accommodations: [],
  getAccommodationStatus: "idle",
  fetchAccommodationsStatus: "idle",
}

export const getAccommodation = createAsyncThunk(
  'accommodation/getAccommodation',
  async ({ accommodationId }: { accommodationId: string }) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/accommodation/${accommodationId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })

    if (!response.ok) {
      const reason = (await response.json()).reason || "unknown_error"
      throw new Error(reason)
    }

    return await response.json()
  }
)

export const fetchAccommodations = createAsyncThunk(
  'accommodation/fetchAccommodations',
  async ({ applicationId }: { applicationId: string }) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/accommodation/application/${applicationId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })

    if (!response.ok) {
      const reason = (await response.json()).reason || "unknown_error"
      throw new Error(reason)
    }

    const data = await response.json()
    return data
  }
)

const accommodationsSlice = createSlice({
  name: 'accommodation',
  initialState,
  reducers: {
    idleAccommodations: (state) => {
      state.getAccommodationStatus = "idle"
      state.getAccommodationError = undefined
      state.fetchAccommodationsStatus = "idle"
      state.fetchAccommodationsError = undefined
    },
  },
  extraReducers: (builder) => {
    builder
    .addCase(getAccommodation.pending, (state) => {
      state.fetchAccommodationsStatus = "loading"
    })
    .addCase(getAccommodation.fulfilled, (state, { payload }: PayloadAction<{ accommodation: ApplicationAccommodation }>) => {
      state.fetchAccommodationsStatus = "success"
      state.accommodation = payload.accommodation
    })
    .addCase(getAccommodation.rejected, (state, action) => {
      state.fetchAccommodationsStatus = "error"
      state.fetchAccommodationsError = action.error.message
    })
      .addCase(fetchAccommodations.pending, (state) => {
        state.fetchAccommodationsStatus = "loading"
      })
      .addCase(fetchAccommodations.fulfilled, (state, { payload }: PayloadAction<{ accommodations: ApplicationAccommodation[], totalNumber: number }>) => {
        state.fetchAccommodationsStatus = "success"
        state.accommodations = payload.accommodations;
        state.accommodationsTotalNumber = payload.totalNumber
      })
      .addCase(fetchAccommodations.rejected, (state, action) => {
        state.fetchAccommodationsStatus = "error"
        state.fetchAccommodationsError = action.error.message
      })
  }
})

export const {
  idleAccommodations
} = accommodationsSlice.actions

export default accommodationsSlice.reducer
