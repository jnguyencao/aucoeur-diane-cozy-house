import Heading from "../../common/Heading"
import "./hero.scss"
import { useTranslation } from 'react-i18next'

const Hero = () => {
  const { t } = useTranslation('home')
  return (
    <>
      <section className='hero'>
        <div className='container'>
        </div>   
      </section>
    </>
  )
}

export default Hero
