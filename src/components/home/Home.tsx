import React from "react"
import Featured from "./featuredServices/Featured"
import Hero from "./hero/Hero"
import Accommodation from "./accommodation/Accommodation"

const Home = () => {
  return (
    <>
      <Hero />
      <Featured />
      <Accommodation />
    </>
  )
}

export default Home
