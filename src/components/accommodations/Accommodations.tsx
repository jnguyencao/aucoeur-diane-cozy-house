import React, { useEffect } from "react"
import RoomCard from "../home/accommodation/RoomCard"
import "../home/accommodation/accommodation.scss"
import { useTranslation } from 'react-i18next'
import APPLICATIONS from "../data/Applications"

const Accommodations = () => {
  const applicationId = APPLICATIONS[0].id
  const { t } = useTranslation('accommodations')
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  
  return (
    <>
      <section className='blog-out mb'>
        <div className="back">
          <div className="container">
            <span className="intro">{t('Our accommodations')}</span>
            <h1 className="intro hide-on-mobile">{t('Our cozy lovely places to stay')}</h1>
          </div>
          <img src='./images/back.jpg' alt="" />
        </div>
        <div className='container accommodation'>
          <RoomCard applicationId={applicationId} />
        </div>
      </section>
    </>
  )
}

export default Accommodations
