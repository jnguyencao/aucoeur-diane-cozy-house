interface Application {
    id: string
    name: string
}

const APPLICATIONS: Application[] = []

if (process.env.REACT_APP_API_URL === "http://localhost:5000") {
  APPLICATIONS.unshift({
    id: 'cosy-local',
    name: "Cosy Nest Realty",
  })
} else if (process.env.REACT_APP_API_URL === "https://aucoeur-back-media.onrender.com") {
  APPLICATIONS.unshift({
    id: 'cosy-demo',
    name: "Cosy Nest Realty",
  })
} else {
  APPLICATIONS.unshift({
    id: 'cosy-prod',
    name: "Cosy Nest Realty",
  })
}

export default APPLICATIONS
