export const featured = [
 {
    cover: "car",
    name: "Car and parking services",
    notice: "Your cars are 100% safe in our parking lots",
    path: ""
  },
  {
    cover: "plane",
    name: "Airport services",
    notice: "Pick up on time from everywhere to anywhere",
    path: ""
  },
  {
    cover: "tourist",
    name: "Tourist services",
    notice: "Our tour guides are always ready to guide you",
    path: ""
  },
]
export const list = [
  {
    _id: "1",
    applicationId: "Diane",
    cover: "../images/list/chambre1.jpg",
    name: "Le Barcarès 1",
    active: true,
    price: 180,
    type: "For 6 people",
    house: "Entier house",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/barcares1/photo1.jpg", "../images/barcares1/photo2.jpg","../images/barcares1/photo3.jpg", "../images/barcares1/photo4.jpg","../images/barcares1/photo5.jpg"]
  },
  {
    _id: "2",
    applicationId: "cosy",
    cover: "../images/list/chambre5.jpg",
    name: "Le Barcarès 2",
    active: true,
    price: 90,
    house: "Apartment",
    type: "For 3 people",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/barcares2/img1.jpg", "../images/barcares2/img2.jpg", "../images/barcares2/img3.jpg"]
  },
  {
    _id: "3",
    applicationId: "cosy",
    cover: "../images/list/chambre3.jpg",
    name: "Saint Mandé",
    active: true,
    price: 50,
    type: "For 2 people",
    house: "Studio",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/saintmande//img1.jpg", "../images/saintmande//img2.jpg", "../images/saintmande/img3.jpg", "../images/saintmande/img4.jpg" ]
  },
  {
    _id: "4",
    applicationId: "cosy",
    cover: "../images/list/chambre4.jpg",
    name: "Paris 17ème 1",
    active: false,
    price: 150,
    type: "For 3 people",
    house: "Apartment",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/paris17-1/img1.jpg","../images/paris17-1/img2.jpg"]
  },
  {
    _id: "5",
    applicationId: "cosy",
    cover: "../images/list/chambre2.jpg",
    name: "Paris 17ème 2",
    active: true,
    price: 180,
    type: "For 2 people",
    house: "Apartment",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/paris17-2/img1.jpg", "../images/paris17-2/img2.jpg", "../images/paris17-2/img3.jpg", "../images/paris17-2/img4.jpg", "../images/paris17-2/img5.jpg", "../images/paris17-2/img6.jpg"]
  },
  {
    _id: "6",
    applicationId: "cosy",
    cover: "../images/list/chambre6.jpg",
    name: "Paris 13ème",
    active: true,
    price: 120,
    type: "For 2 people",
    house: "Studio",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/paris13/img1.jpg", "../images/paris13/img2.jpg", "../images/paris13/img3.jpg", "../images/paris13/img4.jpg"]
  },

  {
    _id: "7",
    applicationId: "cosy",
    cover: "../images/list/chambre7.jpg",
    name: "Paris 7ème",
    active: true,
    price: 1500,
    type: "For 6 people",
    house: "Luxury apartment",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages: ["../images/paris7/img1.jpg", "../images/paris7/img2.jpg", "../images/paris7/img3.jpg", "../images/paris7/img4.jpg", "../images/paris7/img5.jpg", "../images/paris7/img6.jpg", "../images/paris7/img7.jpg", "../images/paris7/img8.jpg", "../images/paris7/img9.jpg"]
  },

  {
    _id: "8",
    applicationId: "cosy",
    cover: "../images/list/chambre8.jpeg",
    name: "Châtillon",
    price: 150,
    active: true,
    house: "Apartment",
    type: "For 4 people",
    numBeds: 2,
    capacity: 2,
    path: "https://drive.google.com/file/d/1UONnbRzGsSAl1noqNyeiVecLe81lSgis/view?usp=sharing",
    additionalImages:["../images/chatilon/img1.jpeg", "../images/chatilon/img2.jpeg", "../images/chatilon/img3.jpeg", "../images/chatilon/img4.jpeg" ]
  },
]

export const footer = [
  {
    title: "PAGES",
    text: [{ list: "Home", path: "/", }, { list: "About", path: "/about", }, { list: "Service", path: "/services", }, { list: "Gallery", path: "/gallery", }, { list: "Accommodations", path: "/accommodations", }, { list: "Contact", path: "/contact", }],
  },
  {
    title: "TELEPHONE",
    text: [{ list: "06 23 21 21 89", path: "", },  { list: "06 65 78 27 76", path: "", },  { list: "06 29 37 48 61", path: "", }],
  },
  {
    title: "FOLLOW US",
    text: [{ list: "aucoeur.service@gmail.com", path: "", }, { list: "Facebook", path: "https://www.facebook.com/dianescozyhouse", }, { list: "Aucoeur Service", path: "https://aucoeurservice.fr", }],
  },
]
