# Getting Started with  React App

First thing first is to copy .env.local.example or env.main.example into your .env

### `npm i` or `npm install` or `npm ci`

## Available Scripts

Then you can run:

### `npm start` or `npm run start`

Runs the app in the development (local) mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
